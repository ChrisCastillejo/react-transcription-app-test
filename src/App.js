import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";

import store from "./store";

import Transcription from "./components/transcription";

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Route exact path="/" component={Transcription} />
      </Router>
    </Provider>
  );
};

export default App;
