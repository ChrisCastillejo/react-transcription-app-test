import React from "react";
import moment from "moment";
import { Icon, Grid, Input, Button } from "semantic-ui-react";
import ReactPlayer from "react-player";
import styles from "../assets/styles";
const TranscriptionView = ({
  transcription,
  handleFieldInput,
  idInput,
  idSegment,
  handleFieldChange,
  progress,
  handleProgress,
  downloadTranscription
}) => {
  return (
    <div style={styles.background}>
      <div>
        <p style={styles.title}>AmberScript</p>
        <Button
          onClick={() =>
            downloadTranscription(
              transcription.recordId,
              transcription.segments
                .map(s => s.words.map(w => w.text).join(" "))
                .join("\r\n\r\n")
            )
          }
          circular
          style={styles.downloadButton}
          size="large"
          icon="cloud download"
        />
      </div>
      <ReactPlayer
        style={styles.reactPlayer}
        config={{ file: { attributes: { controlsList: "nodownload" } } }}
        url="https://s3-eu-west-1.amazonaws.com/public-amb/demo_english_final.mp3"
        controls
        onProgress={e => handleProgress(e)}
        progressInterval={100}
        width="100%"
        height="10%"
      />
      <Grid centered>
        <Grid.Row verticalAlign="middle">
          <Grid.Column width={10} style={styles.transcription}>
            {transcription.segments &&
              transcription.segments.map((segment, index) => (
                <div key={index}>
                  <p style={styles.time}>
                    {`${moment
                      .utc(segment.words[0].start * 1000)
                      .format("HH:mm:ss")} - ${segment.speaker}`}
                  </p>
                  {segment.words.map(
                    (word, subindex) =>
                      idInput === subindex && idSegment === index ? (
                        <span key={subindex}>
                          <Input
                            style={styles.input}
                            onChange={event =>
                              handleFieldChange(
                                index,
                                subindex,
                                event.target.value
                              )
                            }
                            value={word.text}
                          />
                          <Icon
                            name="check"
                            circular
                            style={styles.check}
                            onClick={() => handleFieldInput(false, false)}
                          />
                        </span>
                      ) : (
                        <span
                          key={subindex}
                          style={{
                            fontWeight: progress >= word.start && "bold",
                            textDecoration:
                              progress >= word.start &&
                              progress < word.end &&
                              "underline"
                          }}
                          onClick={() => handleFieldInput(index, subindex)}
                        >{`${word.text} `}</span>
                      )
                  )}
                </div>
              ))}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default TranscriptionView;
