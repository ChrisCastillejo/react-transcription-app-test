export default {
  background: {
    backgroundColor: "#F0F3F4"
  },
  title: {
    fontSize: 40,
    color: "#4FC7D2",
    marginBottom: 20,
    paddingTop: 10,
    textAlign: "center",
    fontWeight: "bold"
  },
  downloadButton: {
    position: "fixed",
    right: 30,
    bottom: 80,
    backgroundColor: "#4FC7D2",
    color: "#fff",
    zIndex: 2,
    boxShadow: "0px 2px 5px 2px #e1e1e1"
  },
  reactPlayer: {
    position: "fixed",
    zIndex: 1,
    bottom: 0
  },
  transcription: {
    textAlign: "justify",
    backgroundColor: "#fff",
    borderRadius: 10,
    boxShadow: "0px 5px 4px 5px #eeeeee",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 40,
    paddingRight: 40,
    marginBottom: 50,
    minHeight: 500
  },
  input: { width: 120 },
  check: {
    marginLeft: 5,
    zIndex: 1,
    cursor: "pointer",
    backgroundColor: "#4FC7D2",
    color: "#fff"
  },
  time: {
    marginBottom: 0,
    color: "#4FC7D2",
    marginTop: 20,
    textAlign: "justify"
  }
};
