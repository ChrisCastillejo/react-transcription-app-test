import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { fetchTranscription } from "../actions";
import TranscriptionView from "./transcription_view";
class Transcription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transcription: this.props.transcription,
      idInput: false,
      idSegment: false,
      progress: 0
    };
    this.props.fetchTranscription();
    this.handleFieldInput = this.handleFieldInput.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleProgress = this.handleProgress.bind(this);
    this.downloadTranscription = this.downloadTranscription.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.transcription, nextProps.transcription)) {
      this.setState({ transcription: nextProps.transcription });
    }
  }
  handleProgress(e) {
    this.setState({ progress: e.playedSeconds });
  }

  handleFieldChange(idSegment, idInput, value) {
    let transcription = this.state.transcription;
    transcription.segments[idSegment].words[idInput].text = value;

    this.setState({ transcription });
  }

  handleFieldInput(idSegment, idInput) {
    this.setState({ idInput, idSegment });
  }

  downloadTranscription(filename, text) {
    var element = document.createElement("a");
    element.setAttribute(
      "href",
      "data:text/plain;charset=utf-8," + encodeURIComponent(text)
    );
    element.setAttribute("download", filename);
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  render() {
    return (
      <TranscriptionView
        transcription={this.state.transcription}
        handleFieldInput={this.handleFieldInput}
        idInput={this.state.idInput}
        idSegment={this.state.idSegment}
        handleFieldChange={this.handleFieldChange}
        progress={this.state.progress}
        handleProgress={this.handleProgress}
        downloadTranscription={this.downloadTranscription}
      />
    );
  }
}

const mapStateToProps = ({ transcription }) => ({
  transcription
});

const actions = {
  fetchTranscription
};

export default connect(mapStateToProps, actions)(Transcription);
