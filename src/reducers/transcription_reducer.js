import { FETCH_TRANSCRIPTION } from "../actions/action_types";

export function transcriptionReducer(state = {}, action) {
  switch (action.type) {
    case FETCH_TRANSCRIPTION: {
      return action.payload;
    }
    default: {
      return state;
    }
  }
}
