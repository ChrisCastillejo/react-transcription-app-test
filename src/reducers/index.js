import { combineReducers } from "redux";
import { transcriptionReducer } from "./transcription_reducer";

const rootReducer = combineReducers({
  transcription: transcriptionReducer
});

export default rootReducer;
