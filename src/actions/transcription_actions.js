import { FETCH_TRANSCRIPTION } from "./action_types";

export function fetchTranscription(name) {
  return dispatch =>
    fetch(
      "https://s3-eu-west-1.amazonaws.com/public-amb/demo_english_final.json"
    )
      .then(response => response.json())
      .then(response => {
        dispatch({ type: FETCH_TRANSCRIPTION, payload: response });
      });
}
